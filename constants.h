#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#define DEFAULT_NUMPIXELS           (30)
#define DEFAULT_NEOPIXEL_DATA_PIN   (2)

#endif