#include "nlights.h"

static uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return Adafruit_NeoPixel::Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return Adafruit_NeoPixel::Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return Adafruit_NeoPixel::Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void NLights::rainbowCycle(unsigned long period) {
  static unsigned long lastUpdate = 0;
  static int state = 0;
  unsigned long now = millis();
  if (now - lastUpdate > period) {
    lastUpdate = now;
  } else {
    return;
  }
//   for(uint16_t j = 0; j < 256 * 5; j++) { // 5 cycles of all colors on wheel
//     for(uint16_t i=0; i< mStrip.numPixels(); i++) {
//       mStrip.setPixelColor(i, Wheel(((i * 256 / mStrip.numPixels()) + j) & 255));
//     }
//     mStrip.show();
//   }
    for(uint16_t j=0; j<256; j++) {
        for(uint16_t i=0; i<mStrip.numPixels(); i++) {
            mStrip.setPixelColor(i, Wheel((i+j) & 255));
        }
        mStrip.show();
    }
}

void NLights::theaterChase(uint32_t c, unsigned long period) {
  static unsigned long lastUpdate = 0;
  static int state = 0;
  unsigned long now = millis();
   
  if (now - lastUpdate > period) {
      lastUpdate = now;
      state++;
      state %= 3;
      Serial.println("updating");
  } else {
      return;
  }

  switch (state) {
      case 0:
        for (int j = 0; j < 10; j++) {  //do 10 cycles of chasing
            for (int q = 0; q < 3; q++) {
                for (uint16_t i = 0; i < mStrip.numPixels(); i = i + 3) {
                     mStrip.setPixelColor(i + q, c);    //turn every third pixel on
                }
                mStrip.show();
            }
        }
        
        break;
      case 1:
        return;
      case 2:
        for (int j = 0; j < 10; j++) {  //do 10 cycles of chasing
            for (int q = 0; q < 3; q++) {
                for (uint16_t i = 0; i < mStrip.numPixels();  i = i + 3) {
                    mStrip.setPixelColor(i + q, 0);        //turn every third pixel off
                }
                // mStrip.show();
            }
        }
        for (int j = 0; j < 10; j++) {  //do 10 cycles of chasing
            for (int q = 0; q < 3; q++) {
                for (uint16_t i = 0; i < mStrip.numPixels(); i = i + 3) {
                     mStrip.setPixelColor(i + q, c);    //turn every third pixel on
                }
                mStrip.show();
            }
        }
        break; 

  };
}

NLights::NLights() {

}

NLights::~NLights() {

}

void NLights::init() {
    mStrip.begin();
}

uint16_t NLights::numberOfSegments() {
    return (uint16_t)mSegments.size();
}

void NLights::setMode(enum Mode m) {
    mMode = m;
}

void NLights::addSegment(Segment s) {
    uint16_t numPixels = 0;

    for (int i = 0; i < mSegments.size(); i++) {
        numPixels += mSegments.getAt(i).pixelCount;
    }

    numPixels += s.pixelCount;
    mSegments.add(s);
    setNumberOfPixels(numPixels);
    updateSegments();
}

void NLights::addSegment(uint16_t pixelCount) {
    uint16_t numPixels = 0;

    for (int i = 0; i < mSegments.size(); i++) {
        numPixels += mSegments.getAt(i).pixelCount;
    }
    numPixels += pixelCount;
    Segment s;
    s.pixelCount = pixelCount;
    mSegments.add(s);
    setNumberOfPixels(numPixels);
}

void NLights::setSegmentColor(uint16_t segmentIndex, uint32_t color) {
    if (segmentIndex >= mSegments.size()) return;
    mSegments.getAt(segmentIndex).color = color;
    uint16_t segmentLength = mSegments.getAt(segmentIndex).pixelCount;
    Serial.print("segment length: ");
    Serial.println(segmentLength);
    uint16_t pixelStartIndex = 0;
    while (segmentIndex > 0) {
        segmentIndex--;
        pixelStartIndex += mSegments.getAt(segmentIndex).pixelCount;
    }
    uint16_t pixelStopIndex = pixelStartIndex + segmentLength;
    Serial.print("pixel start: ");
    Serial.println(pixelStartIndex);
    Serial.print("pixel stop: ");
    Serial.println(pixelStopIndex);
    for (uint16_t i = pixelStartIndex; i < pixelStopIndex; i++) {
        mStrip.setPixelColor(i, color);
    }

    mStrip.show();
}

void NLights::setNumberOfPixels(uint16_t numberOfPixels) {
    mStrip.updateLength(numberOfPixels);
    mStrip.show();
}

uint16_t NLights::numberOfPixels() {
    return mStrip.numPixels();
}

void NLights::setPixelColor(uint16_t pixelIndex, uint32_t pixelColor) {
    mStrip.setPixelColor(pixelIndex, pixelColor);
}

void NLights::update() {
    switch (mMode) {
        case SIMPLE:
        
            break;
        case SEGMENTS:
            
            break;
        case ANIMATIONS:
            rainbowCycle(20);
            break;
    };
}

void NLights::removeAllSegments() {
    setNumberOfPixels(0);
    mSegments.clear();
}

void NLights::updateSegments() {
    for (uint16_t i = 0; i < mSegments.size(); i++) {
        setSegmentColor(i, mSegments.getAt(i).color);
    }
}