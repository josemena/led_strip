#ifndef __VECTOR_H__
#define __VECTOR_H__

template <typename ElemType>
class vector {

public:
	vector();
	~vector();

	int size();
	void add(ElemType s);
	ElemType &getAt(int index);
	void clear();
	ElemType first();
	ElemType last();
    int& operator[] (const int index);

private:
	ElemType *arr;
	int numUsed, numAllocated;
	void doubleCapacity();
};

template <typename ElemType>
vector<ElemType>::vector() {
	arr = new ElemType[10];
	numAllocated = 10;
	numUsed = 0;
}

template <typename ElemType>
vector<ElemType>::~vector() {
	clear();
	delete[] arr;
}

template <typename ElemType>
int vector<ElemType>::size() {
	return numUsed;
}

template <typename ElemType>
void vector<ElemType>::add(ElemType value) {
	if(numUsed == numAllocated) {
		doubleCapacity();
	}
	arr[numUsed++] = value;
}

template <typename ElemType>
ElemType &vector<ElemType>::getAt(int index) {
	return arr[index];
}

template <typename ElemType>
void vector<ElemType>::clear() {
	numAllocated = 0;
}

template <typename ElemType>
ElemType vector<ElemType>::first() {
	return arr[0];
}

template<typename ElemType>
ElemType vector<ElemType>::last() {
	return arr[numUsed - 1];
}

template <typename ElemType>
void vector<ElemType>::doubleCapacity() {
	ElemType *bigger = new ElemType[numAllocated * 2];
	for (int i = 0; i < numUsed; i++) {
		bigger[i] = arr[i];
	}
	delete[] arr;
	arr = bigger;
	numAllocated *= 2;
}

template <typename ElemType>
int& vector<ElemType>::operator[] (const int index) {
    return arr[index];
}

#endif