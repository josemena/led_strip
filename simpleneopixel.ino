// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <aREST.h>
#include <stdio.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#include "nlights.h"


const char *SSID = "tkLABS2G-1";
const char *PASSWORD = "It's 7 O'Clock Somewhere";
#define LISTEN_PORT         (8080)

int pixel_count = 1;
int brightness = 255;


//globals
aREST rest = aREST();
WiFiServer server(LISTEN_PORT);
NLights lights;

NLights::Segment segments[] = {
  {
    4,
    50,
  },
  {
    2,
    0xFF0000,
  },
  {
    5,
    0x00FF00,
  }
};

int delayval = 500; // delay for half a second


//functions available to rest
static int setNumberOfPixels(String command) {
  
  Serial.println(command);
  int newLength = (uint16_t)command.toInt();
  // Serial.print("setting number of pixels to ");
  // Serial.println(newLength);
  // pixels.updateLength(newLength);
  // pixels.show();
}

static int setPixelColor(String command) {
  Serial.println("setting pixel color");

  int index = command.indexOf('_');
  if (index < 0) {
    Serial.println("Error: invalid command");
    return 0;
  }
  uint16_t pixelIndex = (uint16_t)command.substring(0, index).toInt();
  uint32_t pixelColor = (uint32_t)command.substring(index + 1).toInt(); 
  Serial.print("pixel index: ");
  Serial.println(pixelIndex);
  Serial.print("pixel color: ");
  Serial.println(pixelColor);
  
  if (pixelIndex >= lights.numberOfPixels()) {
    Serial.println("Error: invalid number of pixels");
    return 0;
  }
  lights.setPixelColor(pixelIndex, pixelColor);
}

static int setAllColors(String command) {
  Serial.println("setting all pixel colors");
  uint32_t pixelColor = (uint32_t)command.toInt();
  for (uint16_t i = 0; i < lights.numberOfPixels(); i++) {
    lights.setPixelColor(i, pixelColor);
  }
  lights.update();
}

static int setPattern(String command) {
  Serial.println("set a predefined pattern");
}

static int addSegment(String command) {
  Serial.println("adding segment");
  int length = (uint16_t)command.toInt();
  lights.addSegment(length);
  return 0;
}

static int setSegmentColor(String command) {
  
  int index = command.indexOf('_');
  if (index < 0) {
    Serial.println("Error: invalid command");
    return 0;
  }
  uint16_t segmentIndex = (uint16_t)command.substring(0, index).toInt();
  uint32_t segmentColor = (uint32_t)command.substring(index + 1).toInt(); 
  Serial.print("segment index: ");
  Serial.println(segmentIndex);
  Serial.print("segment color: ");
  Serial.println(segmentColor);

  // lights.setSegmentColor(segmentIndex, segmentColor);
  Serial.println("setSegmentColor()");
  Serial.print("command: ");
  Serial.println(command);
  lights.setSegmentColor(segmentIndex, segmentColor);
  return 0;
}

static int clear(String command) {
  Serial.println("clear");
  lights.removeAllSegments();
  return 0;
}

void testLights1() {
  for(int i = 0; i < sizeof(segments) / sizeof(segments[0]); i++) {
    lights.setSegmentColor(i, segments[i].color);
  }
}

void testLights2() {
  for(int i = 0; i < sizeof(segments) / sizeof(segments[0]); i++) {
    lights.setSegmentColor(i, 0x303030);
  }
}

void updateConnectPattern() {
  static uint8_t count = 0;
  if (count++ % 2 == 0) {
    testLights1();
  } else {
    testLights2();
  }
}

static void WIFI_Connect()
{

  WiFi.disconnect();
  Serial.println("Attempting to connect again...");
  // WiFi.mode(WIFI_AP_STA);
  WiFi.begin(SSID, PASSWORD);
    // Wait for connection
  unsigned count = 0;
  while ( WiFi.status() != WL_CONNECTED ) {
      delay(500);
      Serial.print(".");
      updateConnectPattern();
      if (count++ % 20 == 0) {
        Serial.println("");
    }
  }
  

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());
}

void setup() {

  Serial.begin(115200);

  lights.init(); // This initializes the NeoPixel library.

  for(int i = 0; i < sizeof(segments) / sizeof(segments[0]); i++) {
    lights.addSegment(segments[i].pixelCount);
  }

  WIFI_Connect();

  server.begin();

  //setup rest
  rest.set_id("1");
  rest.set_name("tkLABS");
  rest.variable("pixel_count", &pixel_count);
  rest.variable("brightness", &brightness);

  rest.function("setPixelColor", setPixelColor);
  rest.function("setNumberOfPixels", setNumberOfPixels);
  rest.function("setPattern", setPattern);
  rest.function("setAllColors", setAllColors);
  rest.function("addSegment", addSegment);
  rest.function("setSegmentColor", setSegmentColor);
  rest.function("clear", clear);
}



void loop() {

  // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.

  // for(int i=0;i<NUMPIXELS;i++){

  //   // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
  //   pixels.setPixelColor(i, pixels.Color(0,0,150)); // Moderately bright green color.

  //   pixels.show(); // This sends the updated pixel color to the hardware.

  //   delay(delayval); // Delay for a period of time (in milliseconds).

  // }
  static uint32_t count = 0;
  lights.update();
  if (WiFi.status() != WL_CONNECTED) {
    WIFI_Connect();
    return;
  }
  WiFiClient client = server.available();
  if (!client) {
    // if (count++ % 100000 == 0) {
    //   Serial.println("no clients");
    // }
    return;
  } else {
    Serial.println("client found");
  }
  while (!client.available()) {
    delay(1);
  }

  rest.handle(client);

}
