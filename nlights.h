#ifndef __N_LIGHTS_H__
#define __N_LIGHTS_H__

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include "vector.h"
#include "constants.h"

class NLights {
public:

    struct Segment {
        uint16_t pixelCount;
        uint32_t color;
    };

    enum Mode {
        SIMPLE,
        SEGMENTS,
        ANIMATIONS,
        SLIDER,
    };

    NLights();
    ~NLights();
    uint16_t numberOfSegments();
    void setMode(enum Mode mode);

    //segment mode
    void addSegment(Segment s);
    void addSegment(uint16_t pixelCount);
    void setSegmentColor(uint16_t segmentIndex, uint32_t color);
    void removeAllSegments();

    //simple mode
    void setPixelColor(uint16_t pixelIndex, uint32_t pixelColor);
    void setNumberOfPixels(uint16_t numPixels);
    uint16_t numberOfPixels();
    void init();
    void update();

    //animation mode
    void theaterChase(uint32_t c, unsigned long period);
    void rainbowCycle(unsigned long period);

private:
    Mode mMode = SEGMENTS;
    vector<Segment> mSegments;
    Adafruit_NeoPixel mStrip = Adafruit_NeoPixel(DEFAULT_NUMPIXELS, DEFAULT_NEOPIXEL_DATA_PIN, NEO_GRB + NEO_KHZ800);
    void updateSegments();
};

#endif